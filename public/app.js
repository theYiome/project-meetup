const appConfig = {

	mounted: function() {
		console.log(this.time);
		window.setInterval(() => { this.update() }, 1);
	},

	data() {
		return {
			finalDate: new Date('2021-11-17T19:00:00'),
			time: 9999999999,
		};
	},
	
	computed: {
		day: function () {
			return Math.floor(this.time / 24 / 1000 / 60 / 60 );
		},

		hour: function () {
			return Math.floor((this.time / 1000 / 60 / 60) % 24);
		},

		minute: function () {
			return Math.floor(this.time / 1000 / 60 % 60);
		},

		second: function () {
			return Math.floor(this.time / 1000 % 60);
		}
	},

	methods: {

		update() {
			this.time = this.finalDate - new Date();
		},

	}

};

Vue.createApp(appConfig).mount("#app");